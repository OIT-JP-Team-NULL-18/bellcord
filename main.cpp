#include <wiringPi.h>
#include <unistd.h>
#include <string>
#include <string.h>
//#include "Bellcord.h"
#include "common/common.h"
int main(void)
{
	settings mysettings;
	BellcordStatus Current;
	BellcordStatus Old;
	socket_t controller;
	controller.setup_client(mysettings.get_bell_socket.c_str());
	int pin = 2;
	char * temp = nullptr;
	//Setup for wiringPi
	wiringPiSetupGpio();
	pinMode(pin, INPUT);
	pullUpDnControl(pin,PUD_UP);
	while(1)
	{
	//Check bellcord
	Current.Status = digitalRead(pin);
	//Has status changed?
	if (Old.Status != Current.Status)
		{
		//Transmit Bellcord status

        temp = Current.tostr();
		int n = controller.send_msg(temp, strlen(temp) + 1);
		printf("Current: %d, Old: %d, n: %d\n", Current.Status, Old.Status, n);
		}
	Old.Status = Current.Status;

	usleep(250);
	}
	return 0;
}
